<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ujn" uri="http://ujn.edu.cn/common/"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() 
	                   + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML> 
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>客户管理</title>
	<!-- 引入css样式文件 -->
	<!-- Bootstrap Core CSS -->
	<link href="<%=basePath%>css/bootstrap.min.css" rel="stylesheet" />
	<!-- MetisMenu CSS -->
	<link href="<%=basePath%>css/metisMenu.min.css" rel="stylesheet" />
	<!-- DataTables CSS -->
	<link href="<%=basePath%>css/dataTables.bootstrap.css" rel="stylesheet" />
	<!-- Custom CSS -->
	<link href="<%=basePath%>css/sb-admin-2.css" rel="stylesheet" />
	<!-- Custom Fonts -->
	<link href="<%=basePath%>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=basePath%>css/boot-crm.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
  <!-- 导航栏部分 -->
  <nav class="navbar navbar-default navbar-static-top" role="navigation"
		 style="margin-bottom: 0">
	<div class="navbar-header">
		<a class="navbar-brand" href="<%=basePath%>customer/list.action">客户管理系统</a>
	</div>
	<!-- 导航栏右侧图标部分 -->
	<ul class="nav navbar-top-links navbar-right">
	    <!-- 邮件通知 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
				<i><svg t="1604545489876" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="11514" width="20" height="20"><path d="M896 128a128 128 0 0 1 128 128v512a128 128 0 0 1-128 128H128a128 128 0 0 1-128-128V256a128 128 0 0 1 128-128h768z m0 64h-0.064L557.248 530.752a64 64 0 0 1-90.496 0L128 192a64 64 0 0 0-64 64v512a64 64 0 0 0 64 64h768a64 64 0 0 0 64-64V256a64 64 0 0 0-64-64z m-90.624 0H218.56L512 485.504 805.376 192z" p-id="11515" fill="#1296db"></path></svg></i>
			</a>
			<ul class="dropdown-menu dropdown-messages">
				<li>
				    <a href="#">
						<div>
							<strong>张经理</strong> <span class="pull-right text-muted">
								<em>昨天</em>
							</span>
						</div>
						<div>今天晚上开会，讨论一下下个月工作的事...</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a class="text-center" href="#">
				        <strong>查看全部消息</strong>
						
				    </a>
				</li>
			</ul>
		</li>
		<!-- 邮件通知 end -->
		<!-- 任务通知 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
			    <i><svg t="1604545417104" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="10614" width="20" height="20"><path d="M912 226.3v114.3c0 7.7-2.8 14.5-8.5 20.1-5.6 5.6-12.4 8.5-20.1 8.5H140.5c-7.7 0-14.5-2.8-20.1-8.5-5.6-5.6-8.5-12.3-8.5-20.1V226.3c0-7.7 2.8-14.5 8.5-20.1s12.3-8.5 20.1-8.5h742.9c7.7 0 14.5 2.8 20.1 8.5 5.7 5.6 8.5 12.4 8.5 20.1z m0 228.6v114.3c0 7.7-2.8 14.5-8.5 20.1-5.6 5.6-12.4 8.5-20.1 8.5H140.5c-7.7 0-14.5-2.8-20.1-8.5-5.6-5.6-8.5-12.3-8.5-20.1V454.9c0-7.7 2.8-14.5 8.5-20.1 5.6-5.6 12.3-8.5 20.1-8.5h742.9c7.7 0 14.5 2.8 20.1 8.5 5.7 5.7 8.5 12.3 8.5 20.1z m0 228.6v114.3c0 7.7-2.8 14.5-8.5 20.1-5.6 5.6-12.4 8.5-20.1 8.5H140.5c-7.7 0-14.5-2.8-20.1-8.5-5.6-5.6-8.5-12.3-8.5-20.1V683.5c0-7.7 2.8-14.5 8.5-20.1 5.6-5.6 12.3-8.5 20.1-8.5h742.9c7.7 0 14.5 2.8 20.1 8.5 5.7 5.6 8.5 12.4 8.5 20.1z m-514.3-143h457.2v-57.2H397.7v57.2z m171.5 228.7h285.7V712H569.2v57.2zM683.5 312H855v-57.2H683.5V312z" p-id="10615" fill="#1296db"></path></svg></i>
			</a>
			<ul class="dropdown-menu dropdown-tasks">
				<li>
				    <a href="#">
						<div>
							<p>
								<strong>任务 1</strong> 
								<span class="pull-right text-muted">完成40%</span>
							</p>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-success"
									role="progressbar" aria-valuenow="40" aria-valuemin="0"
									aria-valuemax="100" style="width: 40%">
									<span class="sr-only">完成40%</span>
								</div>
							</div>
						</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a href="#">
						<div>
							<p>
								<strong>任务 2</strong> 
								<span class="pull-right text-muted">完成20%</span>
							</p>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-info" role="progressbar"
									aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
									style="width: 20%">
									<span class="sr-only">完成20%</span>
								</div>
							</div>
						</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a class="text-center" href="#"> 
				        <strong>查看所有任务</strong>
						<!--<i class="fa fa-angle-right"></i>-->
				    </a>
				</li>
			</ul> 
		</li>
		<!-- 任务通知 end -->
		<!-- 消息通知 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
				<i><svg t="1604545242514" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="9800" width="20" height="20"><path d="M507.6 960c-75.8 0-147.3-33.3-196.1-91.4-11.4-13.5-9.6-33.7 3.9-45.1 13.5-11.4 33.7-9.6 45.1 3.9C397.1 871 450.7 896 507.6 896c56.9 0 110.5-25 147.1-68.6 11.4-13.5 31.6-15.3 45.1-3.9s15.3 31.6 3.9 45.1C654.9 926.7 583.4 960 507.6 960zM825.2 778.9H198.8c-45.3 0-83.7-27.6-97.7-70.4-13.8-42.3 0.6-86.8 36.7-113.2l44.1-32.2c9.7-7.1 15.5-18.4 15.5-30.2V375.1c0-83.2 32.8-161.3 92.3-220.1C349.1 96.3 428 64 512 64s162.9 32.3 222.3 91c59.5 58.8 92.3 136.9 92.3 220.1v157.7c0 11.8 5.8 23.1 15.5 30.2l44.1 32.2c36.1 26.4 50.5 70.9 36.7 113.2-14 42.9-52.3 70.5-97.7 70.5zM512 128c-138.2 0-250.6 110.8-250.6 247.1v157.7c0 32.2-15.6 62.8-41.7 81.9l-44.1 32.2c-20.4 15-15.6 35.7-13.7 41.7 4.2 12.7 16 26.3 36.8 26.3h626.4c20.8 0 32.7-13.6 36.8-26.3 2-6 6.8-26.8-13.7-41.7l-44.1-32.2c-26.1-19.1-41.7-49.7-41.7-81.9V375.1C762.6 238.8 650.2 128 512 128z" fill="#3688FF" p-id="9801"></path></svg></i>
			</a>
			<ul class="dropdown-menu dropdown-alerts">
				<li>
				    <a href="#">
						<div>
							<i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i> 新回复 
							<span class="pull-right text-muted small">4分钟之前</span>
						</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a href="#">
						<div>
							<i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i> 新消息 
							<span class="pull-right text-muted small">4分钟之前</span>
						</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a href="#">
						<div>
							<i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i> 新任务 
							<span class="pull-right text-muted small">4分钟之前</span>
						</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a href="#">
						<div>
							<i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i> 服务器重启 
							<span class="pull-right text-muted small">4分钟之前</span>
						</div>
				    </a>
				</li>
				<li class="divider"></li>
				<li>
				    <a class="text-center" href="#"> 
				        <strong>查看所有提醒</strong>
						<!--<i class="fa fa-angle-right"></i>-->
				    </a>
				</li>
			</ul> 
		</li>
		<!-- 消息通知 end -->
		<!-- 用户信息和系统设置 start -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
				<i><svg t="1604546824478" class="icon" viewBox="0 0 1126 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="9002" width="20" height="20"><path d="M792.576 379.392a25.6 25.6 0 0 0 25.2928 25.8048h283.2384A25.6 25.6 0 0 0 1126.4 379.392a25.6 25.6 0 0 0-25.2928-25.8048h-283.2384a25.6 25.6 0 0 0-25.344 25.8048z m303.9232 80.7424H761.856c-16.5376 0-29.9008 11.6224-29.9008 25.7536 0 14.1824 13.312 25.7536 29.9008 25.7536h334.6432c16.4864 0 29.9008-11.5712 29.9008-25.7536 0-14.1312-13.4144-25.7536-29.9008-25.7536z m4.608 106.496h-283.2384a25.6 25.6 0 0 0-25.344 25.7536 25.6 25.6 0 0 0 25.344 25.7536h283.2384A25.6 25.6 0 0 0 1126.4 592.384a25.6 25.6 0 0 0-25.2928-25.8048zM543.0272 1024H341.6576C150.8352 1024 0 1024 0 923.648v-20.1216c0-188.16 153.2928-341.1968 341.7088-341.1968h201.2672c188.416 0 341.76 153.0368 341.76 341.1968v20.0704C884.6848 1024 726.3232 1024 542.976 1024z m-203.1616-405.1456c-158.464 0-287.4368 128.4096-287.4368 286.208v20.48c0 40.9088 166.0928 40.9088 287.4368 40.9088h204.9536c100.4544 0 287.4368 0 287.4368-40.96v-20.3776c0-157.8496-128.9728-286.208-287.4368-286.208H339.8656z m92.416-76.7488a271.4112 271.4112 0 0 1-271.2064-271.0528A271.36 271.36 0 0 1 432.2816 0a271.36 271.36 0 0 1 271.2064 271.0528 271.4624 271.4624 0 0 1-271.2064 271.0528z m-215.3472-271.872c0 118.1696 96.6144 214.3232 215.3472 214.3232 118.784 0 215.3984-96.1536 215.3984-214.3232 0-118.2208-96.6144-214.3232-215.3984-214.3232S216.9344 152.0128 216.9344 270.2336z" p-id="9003" fill="#1296db"></path></svg></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i>
				               用户：${user.userName}
				    </a>
				</li>
				<li><a href="#"><i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i> 系统设置</a></li>
				<li class="divider"></li>
				<li>
					<a href="${pageContext.request.contextPath }/outLogin"">
					<i><svg t="1604545917549" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3756" width="20" height="20"><path d="M108.1 190.7h50v50h-50v-50z m187.8 0v50h620v-50h-620zM108.1 537h50v-50h-50v50z m187.8 0h620v-50h-620v50zM108.1 833.3h50v-50h-50v50z m187.8 0h620v-50h-620v50z" fill="#1296db" p-id="3757"></path></svg></i>退出登录
					</a>
				</li>
			</ul> 
		</li>
		<!-- 用户信息和系统设置结束 -->
	</ul>
	<!-- 左侧显示列表部分 start-->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li class="sidebar-search">
					<div class="input-group custom-search-form">
						<input type="text" class="form-control" placeholder="查询内容...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								<i style="padding: 3px 0 3px 0;"><svg t="1604546404649" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7173" width="14" height="14"><path d="M376.5 70c41.4 0 81.5 8.1 119.3 24.1 36.5 15.4 69.3 37.5 97.4 65.7 28.2 28.2 50.3 61 65.7 97.4 16 37.7 24.1 77.9 24.1 119.3s-8.1 81.5-24.1 119.3c-15.4 36.5-37.5 69.3-65.7 97.4-28.2 28.2-61 50.3-97.4 65.7-37.7 16-77.9 24.1-119.3 24.1s-81.5-8.1-119.3-24.1c-36.5-15.4-69.3-37.5-97.4-65.7s-50.3-61-65.7-97.4C78.1 458 70 417.9 70 376.5s8.1-81.5 24.1-119.3c15.4-36.5 37.5-69.3 65.7-97.4 28.2-28.2 61-50.3 97.4-65.7C295 78.1 335.1 70 376.5 70m0-70C168.6 0 0 168.6 0 376.5S168.6 753 376.5 753 753 584.4 753 376.5 584.4 0 376.5 0z" p-id="7174"></path><path d="M274.5 136.1c8.8-8.8 18-17 27.5-24.6-37.3 13.6-72.2 35.3-102.1 65.2-109.7 109.7-109.7 287.7 0 397.4 33.2 33.2 72.6 56.3 114.4 69.4-13.9-10.1-27.2-21.5-39.8-34.1-130.7-130.7-130.7-342.6 0-473.3zM998.3 998.3c-34.2 34.2-90.2 34.2-124.5 0L622.2 746.6c-34.2-34.2-34.2-90.2 0-124.5 34.2-34.2 90.2-34.2 124.5 0l251.7 251.7c34.2 34.3 34.2 90.3-0.1 124.5z" p-id="7175"></path></svg></i>
							</button>
						</span>
					</div> 
				</li>
				<li>
				    <a href="${pageContext.request.contextPath }/customer/list.action" class="active">
				      <i><svg t="1604546138710" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4368" width="20" height="20"><path d="M270.661014 791.939463a36.678644 36.678644 0 0 1-25.982114-10.804038c-10.447726-10.434415-13.614603-26.050714-8.048763-39.761563l3.016367-7.527605c76.137137-190.828675 79.743261-194.434799 82.923448-197.616011L751.257427 107.57144c13.628938-13.792759 38.047579-13.944294 51.978563-0.013311L918.324796 222.60598c6.978802 7.102692 10.763083 16.302302 10.763082 25.982114 0 9.776057-3.811925 18.990001-10.735437 25.954469-0.013311 0.027645-0.040955 0.040955-0.068601 0.068601-1.988385 1.974051-147.913546 147.900235-268.954197 268.926552-85.432992 85.432992-158.457517 158.457517-159.759899 159.746588-2.947766 3.044011-6.4986 6.691091-200.029309 83.938119l-5.086662 2.029341a37.048267 37.048267 0 0 1-13.792759 2.687699z m506.565217-667.078695c-1.316717 0-3.907146 0.328667-6.046043 2.509543L342.424112 556.083383c-0.630713 1.741629-12.599933 27.517942-76.699249 188.168621l-3.071656 7.691426a8.582207 8.582207 0 0 0 1.89214 9.336811c2.495209 2.481898 6.210888 3.290767 9.405411 1.919785l5.168573-2.056986c167.177948-66.717392 189.622538-77.549075 192.515014-79.276369-0.356312 0 72.558657-72.91497 157.840114-158.183116 120.217448-120.204137 264.977427-264.964116 268.967507-268.939862 1.632073-1.645384 2.563809-3.893836 2.563809-6.156623 0-2.248452-0.959381-4.524549-2.632409-6.225223L783.381829 127.411266a8.637497 8.637497 0 0 0-6.155598-2.550498z" p-id="4369" fill="#1296db"></path><path d="M426.430832 654.226887c-3.59179 0-7.197914-1.370982-9.940903-4.112948-5.470619-5.48393-5.470619-14.382517 0.013311-19.867471l383.550514-383.083622c5.48393-5.457309 14.382517-5.470619 19.867471 0.01331 5.470619 5.48393 5.470619 14.382517-0.013311 19.867471L436.358424 650.12725a14.039516 14.039516 0 0 1-9.927592 4.099637z" p-id="4370" fill="#1296db"></path><path d="M479.670821 707.37063a13.995489 13.995489 0 0 1-9.926568-4.112947l-147.173277-147.173276c-5.48393-5.48393-5.48393-14.369207 0-19.853137s14.369207-5.48393 19.853136 0l147.173277 147.173277c5.48393 5.48393 5.48393 14.369207 0 19.853136a13.993441 13.993441 0 0 1-9.926568 4.112947zM313.054986 776.802342a13.995489 13.995489 0 0 1-9.926568-4.112947l-49.797687-49.812021c-5.48393-5.48393-5.48393-14.369207 0-19.853137s14.369207-5.48393 19.853136 0l49.797687 49.812021c5.48393 5.48393 5.48393 14.369207 0 19.853137a13.993441 13.993441 0 0 1-9.926568 4.112947z" p-id="4371" fill="#1296db"></path><path d="M686.637977 926.388916H161.275249c-37.883757 0-65.387365-27.504631-65.387364-65.387364V339.190683c0-21.320364 8.952853-57.078535 68.952533-57.078536h304.534212c7.760027 0 14.039516 6.279489 14.039516 14.039516s-6.279489 14.039516-14.039516 14.039516h-304.534212c-35.923017 0-40.872478 13.944294-40.872478 28.99848v521.810869c0 22.663702 14.643608 37.30731 37.307309 37.30731h525.362728c12.874334 0 25.433311-2.879166 25.433311-37.30731v-308.097333c0-7.760027 6.279489-14.039516 14.040539-14.039516 7.760027 0 14.039516 6.279489 14.039516 14.039516v308.098357c0 54.048859-29.094725 65.387365-53.513366 65.387364z" p-id="4372" fill="#1296db"></path></svg></i> 客户管理
				    </a>
				</li>
				<li>
				    <a href="${pageContext.request.contextPath }/base/list.action">
				      <i><svg t="1604718421387" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="841" width="20" height="20"><path d="M750.5 768.1c-13.8 0-25-11.2-25-25s11.2-25 25-25c43.3 0 78.6-35.3 78.6-78.6V224.2c0-43.4-35.3-78.6-78.6-78.6h-475c-43.4 0-78.6 35.3-78.6 78.6v415.2c0 13.8-11.2 25-25 25s-25-11.2-25-25V224.2c0-70.9 57.7-128.6 128.6-128.6h474.9c70.9 0 128.6 57.7 128.6 128.6v415.2c0.1 71-57.6 128.7-128.5 128.7z" fill="#1296db" p-id="842"></path><path d="M171.9 848.7c-13.8 0-25-11.2-25-25V320.2c0-13.8 11.2-25 25-25s25 11.2 25 25v503.4c0 13.9-11.2 25.1-25 25.1z" fill="#1296db" p-id="843"></path><path d="M854.1 929.6H253.5c-58.3 0-105.8-47.5-105.8-105.8S195.2 718 253.5 718H755c13.8 0 25 11.2 25 25s-11.2 25-25 25H253.5c-30.8 0-55.8 25-55.8 55.8s25 55.8 55.8 55.8h600.6c13.8 0 25 11.2 25 25s-11.2 25-25 25zM406.3 601h-0.8c-8.2-0.3-15.7-4.5-20.1-11.3l-105.1-161c-7.5-11.6-4.3-27.1 7.3-34.6s27.1-4.3 34.6 7.3l85.6 131.1L578.3 305c4.4-5.8 11-9.4 18.3-9.9 7.2-0.5 14.4 2.2 19.5 7.3l127.2 128.2c9.7 9.8 9.7 25.6-0.1 35.4-9.8 9.7-25.6 9.7-35.4-0.1L601 358.1 426.3 591c-4.7 6.4-12.1 10-20 10z" fill="#1296db" p-id="844"></path></svg></i> 数据字典管理
				    </a>
				</li>
			</ul>
		</div>
	</div>
	<!-- 左侧显示列表部分 end--> 
  </nav>
    <!-- 客户列表查询部分  start-->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">客户管理</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-inline" method="get" 
				      action="${pageContext.request.contextPath }/customer/list.action">
					<div class="form-group">
						<label for="customerName">客户名称</label> 
						<input type="text" class="form-control" id="customerName" 
						                   value="${custName }" name="custName" />
					</div>
					<div class="form-group">
						<label for="customerFrom">客户来源</label> 
						<select	class="form-control" id="customerFrom" name="custSource">
							<option value="">--请选择--</option>
							<c:forEach items="${fromType}" var="item">
								<option value="${item.dictId}"
								       <c:if test="${item.dictId == custSource}">selected</c:if>>
								    ${item.dictItemName }
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group">
						<label for="custIndustry">所属行业</label> 
						<select	class="form-control" id="custIndustry"  name="custIndustry">
							<option value="">--请选择--</option>
							<c:forEach items="${industryType}" var="item">
								<option value="${item.dictId}"
								        <c:if test="${item.dictId == custIndustry}"> selected</c:if>>
								    ${item.dictItemName }
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group">
						<label for="custLevel">客户级别</label>
						<select	class="form-control" id="custLevel" name="custLevel">
							<option value="">--请选择--</option>
							<c:forEach items="${levelType}" var="item">
								<option value="${item.dictId}"
								        <c:if test="${item.dictId == custLevel}"> selected</c:if>>
								    ${item.dictItemName }
								</option>
							</c:forEach>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">查询</button>
				</form>
			</div>
		</div>
		<a href="#" class="btn btn-primary" data-toggle="modal" 
		           data-target="#newCustomerDialog" onclick="clearCustomer()">新建</a>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">客户信息列表</div>
					<!-- /.panel-heading -->
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>编号</th>
								<th>客户名称</th>
								<th>客户来源</th>
								<th>客户所属行业</th>
								<th>客户级别</th>
								<th>固定电话</th>
								<th>手机</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${page.rows}" var="row">
								<tr>
									<td>${row.custId}</td>
									<td>${row.custName}</td>
									<td>
									<c:forEach items="${disable}" var="dis">
									<c:if test="${dis.dictItemName == row.custSource}">
									<c:set var="custSource" scope="page" value="0"/>
									</c:if>
									</c:forEach>
									<c:choose>
									<c:when test="${custSource == 0}">数据项被禁用</c:when>
									<c:when test="${custSource != 0}">${row.custSource}</c:when>
									</c:choose>
									<c:set var="custSource" scope="page" value="1"/>
									</td>
									
									<td>
									<c:forEach items="${disable}" var="dis">
									<c:if test="${dis.dictItemName == row.custIndustry}">
									<c:set var="custIndustry" scope="page" value="0"/>
									</c:if>
									</c:forEach>
									<c:choose>
									<c:when test="${custIndustry == 0}">数据项被禁用</c:when>
									<c:when test="${custIndustry != 0}">${row.custIndustry}</c:when>
									</c:choose>
									<c:set var="custIndustry" scope="page" value="1"/>
									</td>
									
									<td><c:forEach items="${disable}" var="dis">
									<c:if test="${dis.dictItemName == row.custLevel}">
									<c:set var="custLevel" scope="page" value="0"/>
									</c:if>
									</c:forEach>
									<c:if test="${custLevel != 0}">${row.custLevel}</c:if>
									<c:if test="${custLevel == 0}">数据项被禁用</c:if>
									<c:set var="custLevel" scope="page" value="1"/>
									</td>
									
									<td>${row.custPhone}</td>
								    <td>${row.custMobile}</td>
									<td>
										<a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#customerEditDialog" onclick= "editCustomer(${row.custId})">修改</a>
										<a href="#" class="btn btn-danger btn-xs" onclick="deleteCustomer(${row.custId})">删除</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					 <div class="col-md-12 text-right">
						<ujn:page url="${pageContext.request.contextPath }/customer/list.action" />
					</div> 
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
	<!-- 客户列表查询部分  end-->
</div>
<!-- 创建客户模态框 -->
<div class="modal fade" id="newCustomerDialog" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">新建客户信息</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="new_customer_form">
					<div class="form-group">
						<label for="new_customerName" class="col-sm-2 control-label">
						    客户名称
						</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_customerName" placeholder="客户名称" name="custName" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_customerFrom" style="float:left;padding:7px 15px 0 27px;">客户来源</label> 
						<div class="col-sm-10">
							<select	class="form-control" id="new_customerFrom" name="custSource">
								<option value="">--请选择--</option>
								<c:forEach items="${fromType}" var="item">
									<option value="${item.dictId}"<c:if test="${item.dictId == custSource}">selected</c:if>>
									${item.dictItemName }									
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="new_custIndustry" style="float:left;padding:7px 15px 0 27px;">所属行业</label>
						<div class="col-sm-10"> 
							<select	class="form-control" id="new_custIndustry"  name="custIndustry">
								<option value="">--请选择--</option>
								<c:forEach items="${industryType}" var="item">
									<option value="${item.dictId}"<c:if test="${item.dictId == custIndustry}"> selected</c:if>>
									${item.dictItemName }
									</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="new_custLevel" style="float:left;padding:7px 15px 0 27px;">客户级别</label>
						<div class="col-sm-10">
							<select	class="form-control" id="new_custLevel" name="custLevel">
								<option value="">--请选择--</option>
								<c:forEach items="${levelType}" var="item">
									<option value="${item.dictId}"<c:if test="${item.dictId == custLevel}"> selected</c:if>>${item.dictItemName }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="new_linkMan" class="col-sm-2 control-label">联系人</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_linkMan" placeholder="联系人" name="custLinkman" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_phone" class="col-sm-2 control-label">固定电话</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_phone" placeholder="固定电话" name="custPhone" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_mobile" class="col-sm-2 control-label">移动电话</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_mobile" placeholder="移动电话" name="custMobile" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_zipcode" class="col-sm-2 control-label">邮政编码</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_zipcode" placeholder="邮政编码" name="custZipcode" />
						</div>
					</div>
					<div class="form-group">
						<label for="new_address" class="col-sm-2 control-label">联系地址</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="new_address" placeholder="联系地址" name="custAddress" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="createCustomer()">创建客户</button>
			</div>
		</div>
	</div>
</div>
<!-- 修改客户模态框 -->
<div class="modal fade" id="customerEditDialog" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">修改客户信息</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="edit_customer_form">
					<input type="hidden" id="edit_cust_id" name="custId"/>
					<div class="form-group">
						<label for="edit_customerName" class="col-sm-2 control-label">客户名称</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_customerName" placeholder="客户名称" name="custName" />
						</div>
					</div>
					<div class="form-group">
						<label for="edit_customerFrom" style="float:left;padding:7px 15px 0 27px;">客户来源</label> 
						<div class="col-sm-10">
							<select	class="form-control" id="edit_customerFrom" name="custSource">
								<option value="">--请选择--</option>
								<c:forEach items="${fromType}" var="item">
									<option value="${item.dictId}"<c:if test="${item.dictId == custSource}"> selected</c:if>>${item.dictItemName }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="edit_custIndustry" style="float:left;padding:7px 15px 0 27px;">所属行业</label>
						<div class="col-sm-10"> 
							<select	class="form-control" id="edit_custIndustry"  name="custIndustry">
								<option value="">--请选择--</option>
								<c:forEach items="${industryType}" var="item">
									<option value="${item.dictId}"<c:if test="${item.dictId == custIndustry}"> selected</c:if>>${item.dictItemName }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="edit_custLevel" style="float:left;padding:7px 15px 0 27px;">客户级别</label>
						<div class="col-sm-10">
							<select	class="form-control" id="edit_custLevel" name="custLevel">
								<option value="">--请选择--</option>
								<c:forEach items="${levelType}" var="item">
									<option value="${item.dictId}"<c:if test="${item.dictId == custLevel}"> selected</c:if>>${item.dictItemName }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="edit_linkMan" class="col-sm-2 control-label">联系人</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_linkMan" placeholder="联系人" name="custLinkman" />
						</div>
					</div>
					<div class="form-group">
						<label for="edit_phone" class="col-sm-2 control-label">固定电话</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_phone" placeholder="固定电话" name="custPhone" />
						</div>
					</div>
					<div class="form-group">
						<label for="edit_mobile" class="col-sm-2 control-label">移动电话</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_mobile" placeholder="移动电话" name="custMobile" />
						</div>
					</div>
					<div class="form-group">
						<label for="edit_zipcode" class="col-sm-2 control-label">邮政编码</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_zipcode" placeholder="邮政编码" name="custZipcode" />
						</div>
					</div>
					<div class="form-group">
						<label for="edit_address" class="col-sm-2 control-label">联系地址</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="edit_address" placeholder="联系地址" name="custAddress" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="updateCustomer()">保存修改</button>
			</div>
		</div>
	</div>
</div>
<!-- 引入js文件 -->
<!-- jQuery -->
<script src="<%=basePath%>js/jquery-1.11.3.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>js/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="<%=basePath%>js/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>js/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>js/sb-admin-2.js"></script>
<!-- 编写js代码 -->
<script type="text/javascript">
//清空新建客户窗口中的数据
	function clearCustomer() {
	    $("#new_customerName").val("");
	    $("#new_customerFrom").val("")
	    $("#new_custIndustry").val("")
	    $("#new_custLevel").val("")
	    $("#new_linkMan").val("");
	    $("#new_phone").val("");
	    $("#new_mobile").val("");
	    $("#new_zipcode").val("");
	    $("#new_address").val("");
	}
	// 创建客户
	function createCustomer() {
	$.post("<%=basePath%>customer/create.action",
	$("#new_customer_form").serialize(),function(data){
	        if(data =="OK"){
	            alert("客户创建成功！");
	            window.location.reload();
	        }else{
	            alert("客户创建失败！");
	            window.location.reload();
	        }
	    });
	}
	// 通过id获取修改的客户信息
	function editCustomer(id) {
	    $.ajax({
	        type:"get",
	        url:"<%=basePath%>customer/getCustomerById.action",
	        data:{"id":id},
	        success:function(data) {
	            $("#edit_cust_id").val(data.custId);
	            $("#edit_customerName").val(data.custName);
	            $("#edit_customerFrom").val(data.custSource)
	            $("#edit_custIndustry").val(data.custIndustry)
	            $("#edit_custLevel").val(data.custLevel)
	            $("#edit_linkMan").val(data.custLinkman);
	            $("#edit_phone").val(data.custPhone);
	            $("#edit_mobile").val(data.custMobile);
	            $("#edit_zipcode").val(data.custZipcode);
	            $("#edit_address").val(data.custAddress);
	            
	        }
	    });
	}
    // 执行修改客户操作
	function updateCustomer() {
		$.post("<%=basePath%>customer/update.action",$("#edit_customer_form").serialize(),function(data){
			if(data =="OK"){
				alert("客户信息更新成功！");
				window.location.reload();
			}else{
				alert("客户信息更新失败！");
				window.location.reload();
			}
		});
	}
	// 删除客户
	function deleteCustomer(id) {
	    if(confirm('确实要删除该客户吗?')) {
	$.post("<%=basePath%>customer/delete.action",{"id":id},
	function(data){
	            if(data =="OK"){
	                alert("客户删除成功！");
	                window.location.reload();
	            }else{
	                alert("删除客户失败！");
	                window.location.reload();
	            }
	        });
	    }
	}
</script>
</body>
</html>