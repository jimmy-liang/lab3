package cn.edu.ujn.lab3.c;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.edu.ujn.lab3.dao.BaseDict;
import cn.edu.ujn.lab3.dao.Customer;
import cn.edu.ujn.lab3.dao.QueryVo;
import cn.edu.ujn.lab3.dao.QueryVoBaseDict;
import cn.edu.ujn.lab3.dao.User;
import cn.edu.ujn.lab3.service.ICustomerService;
import cn.edu.ujn.lab3.util.Page;

@Controller
public class CustomerController {

	@Autowired
	private ICustomerService customerService;

	// 添加客户
	@ResponseBody
	@RequestMapping("/customer/create.action")
	public String add(Customer customer, HttpSession session) {
		// 获取已登录用户
		User user = (User) session.getAttribute("user");
		// 设置创建人id
		customer.setCustCreateId(user.getUserId());
		// 获取当前时间
		Date date = new Date();
		Timestamp timeStamp = new Timestamp(date.getTime());
		// 设置创建时间
		customer.setCustCreatetime(timeStamp);
		// 判断是否插入成功
		int row = this.customerService.insert(customer);
		if (row > 0) {
			return "OK";
		} else {
			return "FL";
		}
	}

	@RequestMapping("/customer/list.action")
	public String list(QueryVo vo, Model model) {
		// 客户来源
		List<BaseDict> sourceList = customerService.findDictByCode("002");
		// 客户行业
		List<BaseDict> industryList = customerService.findDictByCode("001");
		// 客户级别
		List<BaseDict> levelList = customerService.findDictByCode("006");
		// 获取停用的数据项
		List<BaseDict> disableList = customerService.findAlldisable();

		// 默认当前页为1
		if (vo.getPage() == null) {
			vo.setPage(1);
		} // 设置查询的起始记录条数

		vo.setStart((vo.getPage() - 1) * vo.getSize());

		List<Customer> findCustomerByVo = customerService.findCustomerByVo(vo);
		Integer count = customerService.findCustomerByVoCount(vo);

		Page<Customer> page = new Page<Customer>();
		page.setTotal(count); // 数据总数
		page.setSize(vo.getSize());// 每页显示条数
		page.setPage(vo.getPage());// 当前页数

		page.setRows(findCustomerByVo); // 数据列表
		// 将查询结果返回给页面
		model.addAttribute("page", page);
		// 页面数据下拉列表显示
		model.addAttribute("fromType", sourceList);
		model.addAttribute("industryType", industryList);
		model.addAttribute("levelType", levelList);
		model.addAttribute("disable", disableList);
		// 数据回显
		model.addAttribute("custName", vo.getCustName());
		model.addAttribute("custSource", vo.getCustSource());
		model.addAttribute("custIndustry", vo.getCustIndustry());
		model.addAttribute("custLevel", vo.getCustLevel());
		return "customer";
	}
	
    //刪除客戶
	@ResponseBody
	@RequestMapping("/customer/delete.action")
	public String delete(int id) throws Exception {
		customerService.deleteByPrimaryKey(id);
		return "OK";
	}

	@ResponseBody
	@RequestMapping(value = "customer/getCustomerById.action")
	public Customer edit(Integer id) {

		return this.customerService.selectByPrimaryKey(id);

	}

	@ResponseBody
	@RequestMapping(value = "customer/update.action")
	public String update(Customer record) {

		this.customerService.updateByPrimaryKey(record);

		return "OK";

	}

	@RequestMapping(value = "/base/list.action")
	public String basedict(QueryVoBaseDict vobasedict, Model model) {
		List<BaseDict> type = customerService.findAllDictTypeName();
		List<BaseDict> item = customerService.findAllDictItemName();
		List<BaseDict> enable = customerService.findAllDictEnable();

		if (vobasedict.getPage() == null) {
			vobasedict.setPage(1);
		} // 设置查询的起始记录条数

		vobasedict.setStart((vobasedict.getPage() - 1) * vobasedict.getSize());

		List<BaseDict> findBaseDictByVoBaseDict = customerService.findBaseDictByVoBaseDict(vobasedict);
		Integer count = customerService.findCustomerByVoBaseDictCount(vobasedict);

		Page<BaseDict> page = new Page<BaseDict>();
		page.setTotal(count); // 数据总数
		page.setSize(vobasedict.getSize());// 每页显示条数
		page.setPage(vobasedict.getPage());// 当前页数
		page.setRows(findBaseDictByVoBaseDict); // 数据列表
		model.addAttribute("page", page);

		model.addAttribute("NameType", type);
		model.addAttribute("ItemType", item);
		model.addAttribute("EnableType", enable);

		model.addAttribute("dictTypeCode", vobasedict.getDictTypeCode());
		model.addAttribute("dictTypeName", vobasedict.getDictTypeName());
		model.addAttribute("dictItemName", vobasedict.getDictItemName());
		model.addAttribute("dictEnable", vobasedict.getDictEnable());
		return "basedict";
	}

	@ResponseBody
	@RequestMapping("/basedict/create.action")
	public String add(BaseDict basedict) {

		int row = this.customerService.insertBaseDict(basedict);
		if (row > 0) {
			return "OK";
		} else {
			return "FL";
		}
	}

	@ResponseBody
	@RequestMapping(value = "basedict/getBasedictById.action")
	public BaseDict editBaseDict(String id) {

		return this.customerService.findBaseDictById(id);
	}

	@ResponseBody
	@RequestMapping(value = "basedict/update.action")
	public String updatebase(BaseDict record) {

		int i = this.customerService.updateBaseDict(record);

		if (i > 0)
			return "OK";
		else
			return "fl";

	}

	@ResponseBody
	@RequestMapping("BaseDict/delete.action")
	public String deleteBaseDict(String id) throws Exception {

		customerService.updateBySource(id);
		customerService.updateByIndustry(id);
		customerService.updateByLevel(id);
		// System.out.println("ddictId:"+id);
		int deleteByPrimaryKey = customerService.deleteByPrimaryKey(id);
		// System.out.println("deleteByPrimaryKey:"+deleteByPrimaryKey);
		return "OK";
	}
}
