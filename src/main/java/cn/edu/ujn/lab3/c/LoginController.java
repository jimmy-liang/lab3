package cn.edu.ujn.lab3.c;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.edu.ujn.lab3.dao.BaseDict;
import cn.edu.ujn.lab3.dao.User;
import cn.edu.ujn.lab3.service.IUserService;

@Controller
public class LoginController {

	@Autowired
	private IUserService userservice;
	@RequestMapping("/tologin")
	public String tologin() {
		return "login";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
      public String login(String usercode,String password,Model model,HttpSession session ) {
		  User user = this.userservice.findUserForLogin(usercode, password);
		  if(user!=null) {
			  session.setAttribute("user", user);
			  //model.addAttribute("user",user);
			  return "redirect:customer/list.action";
		  }
		  model.addAttribute("msg","账号或密码错误，请重新输入！");
    	  return "login";
      }
	
	@RequestMapping("/outLogin")
	public String outLogin(HttpSession session){
		//通过session.invalidata()方法来注销当前的session
		session.invalidate();
		return "login";
	}
	
	@RequestMapping("/toMain")
	public String toCustomer() {
	    return "main";
	}

}
