package cn.edu.ujn.lab3.service;

import cn.edu.ujn.lab3.dao.User;

public interface IUserService {

	public User findUserForLogin(String usercode,String password);
	
	
}
