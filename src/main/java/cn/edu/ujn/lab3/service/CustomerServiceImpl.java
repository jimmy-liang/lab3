package cn.edu.ujn.lab3.service;

import java.util.List;

import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.edu.ujn.lab3.dao.BaseDict;
import cn.edu.ujn.lab3.dao.BaseDictMapper;
import cn.edu.ujn.lab3.dao.Customer;
import cn.edu.ujn.lab3.dao.CustomerMapper;
import cn.edu.ujn.lab3.dao.QueryVo;
import cn.edu.ujn.lab3.dao.QueryVoBaseDict;
@Transactional
@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerMapper customerMapper;
	@Autowired
	private BaseDictMapper basedictMapper;

	@Override
	public int insert(Customer record) {
		// TODO Auto-generated method stub
		int insert = this.customerMapper.insert(record);
		return insert;
	}
	
	//插入数据字典
	   
		@Override
		public int insertBaseDict(BaseDict record) {
		
			//
			String dictId = basedictMapper.findMaxDictId();
			//
			int a=Integer.parseInt(dictId);
			a = a+1;
			dictId=a+"";   
			//
			record.setDictId(dictId);
			//
			String dictTypeCode = this.basedictMapper.selectTypeCodeByTypeName(record.getDictTypeName());
			if(dictTypeCode == null || "".equals(dictTypeCode)) {
				dictTypeCode = basedictMapper.findMaxDictTypeCode();
				int b=Integer.parseInt(dictTypeCode);
				b = b+1;
				dictTypeCode="0"+b+"";
				record.setDictTypeCode(dictTypeCode);
				record.setDictSort(1);
	
			}
			else {
				record.setDictTypeCode(dictTypeCode);
				int dictSort = basedictMapper.findMaxDictSort(record.getDictTypeName())+1;
				record.setDictSort(dictSort);
			}
			
			//
			
			//
			record.setDictEnable("1");
			
			int insert = this.basedictMapper.insert(record);
			

			return insert;
		}
		
	@Override
	public List<BaseDict> findDictByCode(String dictTypeCode) {
		// TODO Auto-generated method stub
		List<BaseDict> list = this.basedictMapper.findDictByCode(dictTypeCode);
		return list;
	}
	@Override
	public List<Customer> findCustomerByVo(QueryVo vo) {
		// TODO Auto-generated method stub
		List<Customer> findCustomerByVo = customerMapper.findCustomerByVo(vo);
		return findCustomerByVo;
	}
	@Override
	public void deleteByPrimaryKey(int id) {
		customerMapper.deleteByPrimaryKey(id);
	}
	
	
	@Override
	public Customer selectByPrimaryKey(int id) {
		
		return this.customerMapper.selectByPrimaryKey(id);
	}
	@Override
	public int updateByPrimaryKey(Customer record) {
		// TODO Auto-generated method stub
		return this.customerMapper.updateByPrimaryKey(record);
	}
	@Override
    public Integer findCustomerByVoCount(QueryVo vo) {
        Integer count = customerMapper.findCustomerByVoCount(vo);
        return count;
    }
	@Override
	public List<BaseDict> findAllDictTypeName() {
		// TODO Auto-generated method stub
		List<BaseDict> dict = basedictMapper.findAllDictTypeName();
		return dict;
	}
	@Override
	public List<BaseDict> findAllDictItemName() {
		// TODO Auto-generated method stub
		List<BaseDict> dict = basedictMapper.findAllDictItemName();
		return dict;
	}
	@Override
	public List<BaseDict> findAllDictEnable() {
		// TODO Auto-generated method stub
		List<BaseDict> dict = basedictMapper.findAllDictEnable();
		return dict;
	}

	@Override
	public List<BaseDict> findBaseDictByVoBaseDict(QueryVoBaseDict vobasedict) {
		// TODO Auto-generated method stub
		List<BaseDict> list = basedictMapper.findBaseDictByVoBaseDict(vobasedict);
		return list;
	}

	@Override
	public Integer findCustomerByVoBaseDictCount(QueryVoBaseDict vobasedict) {
		// TODO Auto-generated method stub
		Integer count = basedictMapper.findCustomerByVoBaseDictCount(vobasedict);
		return count;
	}

	@Override
	public BaseDict findBaseDictById(String id) {
		// TODO Auto-generated method stub
		return basedictMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateBaseDict(BaseDict record) {
		// TODO Auto-generated method stub
		return basedictMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int deleteByPrimaryKey(String dictId) {
		// TODO Auto-generated method stub
			int deleteByPrimaryKey = basedictMapper.deleteByPrimaryKey(dictId);
		return deleteByPrimaryKey;
	}

	@Override
	public void updateBySource(String id) {
		// TODO Auto-generated method stub
		customerMapper.updateBySource(id);
	}

	@Override
	public void updateByIndustry(String id) {
		// TODO Auto-generated method stub
		customerMapper.updateByIndustry(id);
	}

	@Override
	public void updateByLevel(String id) {
		// TODO Auto-generated method stub
		customerMapper.updateByLevel(id);
	}
	

	@Override
	public List<BaseDict> findAlldisable() {
		// TODO Auto-generated method stub
		return basedictMapper.findAlldisable();
	}
}
