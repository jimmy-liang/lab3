package cn.edu.ujn.lab3.service;

import java.util.List;

import cn.edu.ujn.lab3.dao.BaseDict;
import cn.edu.ujn.lab3.dao.Customer;
import cn.edu.ujn.lab3.dao.QueryVo;
import cn.edu.ujn.lab3.dao.QueryVoBaseDict;

public interface ICustomerService {

	//插入客户
	public int insert(Customer record);
	
	//插入数据字典
	public int insertBaseDict(BaseDict record);
	
	
	public List<BaseDict> findDictByCode(String dictTypeCode);
	
	public List<Customer> findCustomerByVo(QueryVo vo);
	
	public void deleteByPrimaryKey(int id);
	
	public Customer selectByPrimaryKey(int id);
	
	int updateByPrimaryKey(Customer record);
	
	Integer findCustomerByVoCount(QueryVo vo); 
	
	public List<BaseDict> findAllDictTypeName();
	
	public List<BaseDict> findAllDictItemName();
	
	public List<BaseDict> findAllDictEnable();
	
	public List<BaseDict> findBaseDictByVoBaseDict(QueryVoBaseDict vobasedict);
    
    public Integer findCustomerByVoBaseDictCount(QueryVoBaseDict vobasedict);
    
    public BaseDict findBaseDictById(String id); 
    
    public int updateBaseDict(BaseDict record);
    
    int deleteByPrimaryKey(String dictId);
    
    void updateBySource(String id);
    
    void updateByIndustry(String id);
    
    void updateByLevel(String id);
    
    public List<BaseDict> findAlldisable();
}
