package cn.edu.ujn.lab3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.edu.ujn.lab3.dao.User;
import cn.edu.ujn.lab3.dao.UserMapper;
@Service
@Transactional
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserMapper usermapper;
	@Override
	public User findUserForLogin(String usercode, String password) {
		// TODO Auto-generated method stub
		User user = this.usermapper.findUserForLogin(usercode, password);
		return user;
	}

}
 