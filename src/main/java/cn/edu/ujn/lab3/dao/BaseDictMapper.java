package cn.edu.ujn.lab3.dao;

import java.util.List;

import cn.edu.ujn.lab3.dao.BaseDict;

public interface BaseDictMapper {
    int deleteByPrimaryKey(String dictId);

    int insert(BaseDict record);

    int insertSelective(BaseDict record);
    
    
    int findMaxDictSort(String dictTypeName);
    
    //��ѯ�������
    String findMaxDictId();
    
    String findMaxDictTypeCode();

    BaseDict selectByPrimaryKey(String dictId);
    
    //����������Ʋ�ѯ������
    String selectTypeCodeByTypeName(String dictTypeName);

    int updateByPrimaryKeySelective(BaseDict record);

    int updateByPrimaryKey(BaseDict record);
    
    int updateByPrimaryKey1(BaseDict record);
    
    List<BaseDict> findDictByCode(String dictTypeCode);
    
    List<BaseDict> findAllDictTypeName();
    
    List<BaseDict> findAllDictItemName();
    
    List<BaseDict> findAllDictEnable();
    
    List<BaseDict> findBaseDictByVoBaseDict(QueryVoBaseDict vobasedict);
    
    Integer findCustomerByVoBaseDictCount(QueryVoBaseDict vobasedict);
    
    List<BaseDict> findAlldisable();
}