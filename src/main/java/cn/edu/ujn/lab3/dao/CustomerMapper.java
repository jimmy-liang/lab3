package cn.edu.ujn.lab3.dao;

import java.util.List;

import cn.edu.ujn.lab3.dao.Customer;

public interface CustomerMapper {
    int deleteByPrimaryKey(Integer custId);

    int insert(Customer record);

    int insertSelective(Customer record);

    Customer selectByPrimaryKey(Integer custId);

    int updateByPrimaryKeySelective(Customer record);

    int updateByPrimaryKey(Customer record);
    
    List<Customer> findCustomerByVo(QueryVo vo);
    
    Integer findCustomerByVoCount(QueryVo vo);
    
    int deleteByKey(String Id);
    
    void updateBySource(String id);
    
    void updateByIndustry(String id);
    
    void updateByLevel(String id);
}