package cn.edu.ujn.lab3.dao;

public class QueryVoBaseDict {
	
	private String dictTypeCode;
	
	private String dictTypeName;
	
	private String dictItemName;
	
	private String dictEnable;
	
	private Integer page=1;
	
	private Integer start;
	
	private Integer size=10;

	public String getDictTypeCode() {
		return dictTypeCode;
	}

	public void setDictTypeCode(String dictTypeCode) {
		this.dictTypeCode = dictTypeCode;
	}

	public String getDictTypeName() {
		return dictTypeName;
	}

	public void setDictTypeName(String dictTypeName) {
		this.dictTypeName = dictTypeName;
	}

	public String getDictItemName() {
		return dictItemName;
	}

	public void setDictItemName(String dictItemName) {
		this.dictItemName = dictItemName;
	}

	public String getDictEnable() {
		return dictEnable;
	}

	public void setDictEnable(String dictEnable) {
		this.dictEnable = dictEnable;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "QueryVoBaseDict [dictTypeCode=" + dictTypeCode + ", dictTypeName=" + dictTypeName + ", dictItemName="
				+ dictItemName + ", dictEnable=" + dictEnable + ", page=" + page + ", start=" + start + ", size=" + size
				+ "]";
	}
	
	

}
