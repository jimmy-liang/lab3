package cn.edu.ujn.lab3.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;

import cn.edu.ujn.lab3.dao.User;

public class UserInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		//获取url
		String url = request.getRequestURI();
		//除了登录请求外，其他的URL都被拦截
	    if(url.indexOf("/login")>=0) {
	    	return true;
	    }
	    if(url.indexOf("/tologin")>=0) {
	    	return true;
	    }
	    HttpSession session = request.getSession();
	    User user = (User)session.getAttribute("user");
	    if(user!=null) {
	    	return true;
	    }
	    request.setAttribute("msg", "您还没有登录请先登录");
	    request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
		return false;

	    
	}
}
